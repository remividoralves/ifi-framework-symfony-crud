<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Article;
use App\Form\ArticleType;

class DefaultController extends AbstractController
{

    public function index()
    {
        return new Response('
            <html>
                <body>
                    <h1>Hello Symfony 4 World</h1>
                </body>
            </html>
        ');
    }
    
    public function read()
    {
        //TODO
    }

    public function create(Request $request) : Response{
        //TODO
    }

    public function delete(int $id){
        //TODO
    }

    public function update(Request $request, int $id){
        //TODO
    }

}