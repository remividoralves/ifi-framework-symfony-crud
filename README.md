**Pratique du Framework Symfony**

**Objectif du TP :** Créer un CRUD d'articles

Ici vous trouverez la base du projet   
Copier Coller le ficher dans le dossier wamp/www/  

**Prérequis :**  
-Avoir Installer Php  
-Avoir Installer Composer   
-Avoir Installer Symfony  
-Avoir Installer wamp (windows)  

**Pour vérifier le bon Comportement du TP :**    
-Dans votre cmd : dans le projet CRUD Symfony  
-Taper Symfony server:start   
-Rendez vous sur "http://localhost:8000/"  
-Verifier le Hello World  

**Etape 1 :**   
Créer l'entity Article  
(Ici vous pouvez completer la classe Article présent dans le dossier Entity || si vous avez bien tout installer taper la commande php bin/console make:entity Article)

**Etape 2:**  
Créer le formulaire permettant de créer des articles  
(Ici, le formulaire est déja ajouter dans le projet, il vous faudra completer la classe ArticleForm afin d'ajouter les données que vous avez mis dans la classe Article)  

**Etape 3:**  
Dans Symfony, le routage est géré dans le fichier Routes.yaml, vous y trouverez un exemple. Il est aussi possible de créer les routes directement dans le Controller grâce aux annotations, si vous préferez cette méthode, libre à vous de faire comme bon vous semble.  
  
**Etape 4:**  
Vous trouvez le Controller Default Controller dans le dossier src/Controller, vous aurez à implementer chaque methode Read/Create/Update/Delete du CRUD

**Etape 5:**  
Un dossier Templates se trouve dans le projet, cest ici que se trouve les views en Symfony.  
Le fichier base.html.twig n'est pas à modifier et contient un block body dans lequel vous devez ajouter le code nécessaire aux views de creation et d'affichage.

**Etape 6:**  
Si tout s'est bien passé jusque la, il vous faudra faire quelques commandes afin de vérifier le fonctionnement du CRUD, notamment pour sauvegarder les articles en BDD  
voici les deux commandes qui permettront de créer la base dans phpmyadmin  
-php bin/console make:migration   
-php bin/console doctrine:migrations:migrate  

**Etape 6:**  
Tester votre CRUD et vérifier son bon fonctionnement, Voila vous avez développer votre premier projet Symfony.  

**Etape Bonus:**  
Vous avez finis le TP ? Vous voulez en savoir un peu plus sur les repositories?   
On va ajouter quelques méthodes dans le ArticleRepository :   
1/ Créer une méthode qui permet de rechercher les articles qui ont un prix supérieur a 100 euros   
2/ Créer une méthode qui permet de rechercher les articles qui appartiennent uniquement à la catégorie X  
Pour vérifier si cela fonctionne, dans votre controller, à la place de Findall(), utiliser la méthode implémenter (1 ou 2) 